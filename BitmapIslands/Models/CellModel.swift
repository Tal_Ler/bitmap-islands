//
//  Cell.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 31/03/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import UIKit

struct CellModel {
    var value: Int
    
    init(value: Int) {
        self.value = value
    }
    
    mutating func selectCell() {
        switch value {
        case 0:
            value = 1
        default:  // case 1:
            value = 0
        }
    }
}
