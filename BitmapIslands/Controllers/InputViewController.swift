//
//  InputViewController.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 31/03/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import UIKit

protocol InputViewControllerCoordinatorDelegate: class {
    func randomButtonTapped()
    func drawButtonTapped()
}

class InputViewController: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var explenationLabel:     UILabel?
    @IBOutlet weak var nBitmapSizeTextField: UITextField?
    @IBOutlet weak var mBitmapSizeTextField: UITextField?
    @IBOutlet weak var randomButton:         UIButton?
    @IBOutlet weak var drawButton:           UIButton?
    
    // MARK: - Properties
    weak var coordinatorDelegate: InputViewControllerCoordinatorDelegate?
    private let maxBitmapSize = 1000
    var bitmap: Bitmap?
    
    // MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nBitmapSizeTextField?.delegate = self
        nBitmapSizeTextField?.keyboardType = .numberPad
        mBitmapSizeTextField?.delegate = self
        mBitmapSizeTextField?.keyboardType = .numberPad
        randomButton?.layer.cornerRadius = 5
        drawButton?.layer.cornerRadius = 5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        nBitmapSizeTextField?.text = ""
        mBitmapSizeTextField?.text = ""
    }
    
    // MARK: - IBAction
    @IBAction func randomButtonTapped(_ sender: Any) {
        beforeMoveNextScreen()
        
        // update coordinator for navigation
        coordinatorDelegate?.randomButtonTapped()
    }
    
    @IBAction func drawButtonTapped(_ sender: Any) {
        beforeMoveNextScreen()
        
        // update coordinator for navigation
        coordinatorDelegate?.drawButtonTapped()
    }
    
    private func beforeMoveNextScreen() {
        // update bitmapModel
        if let nText = nBitmapSizeTextField?.text,
            let n = Int(nText),
            let mText = mBitmapSizeTextField?.text,
            let m = Int(mText){
            bitmap = Bitmap(size: CGSize(width: n, height: m) )
        } else {
            // defoult size
            bitmap = Bitmap(size: CGSize(width: 10, height: 10) )
        }
        // close keyboard
        nBitmapSizeTextField?.resignFirstResponder()
        mBitmapSizeTextField?.resignFirstResponder()
    }
}

// MARK: - StoryboardInstantiableB
extension InputViewController: StoryboardInstantiableB {}

// MARK: - UITextFieldDelegate
extension InputViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var res = true
        var newText = string
        if let text = textField.text {
            newText = text + string
        }
        let number = Int(newText) ?? 0
        if (number == 0) {
            res = false
            textField.text = ""
        }
        else if (number > maxBitmapSize) {
            res = false
            textField.text = "\(maxBitmapSize)"
        }

        return res
    }

}
