//
//  ColorModel.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 02/04/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import UIKit

struct ColorModel {
    
    private var colorDictionary: [Int: UIColor] = [0: .white, 1: .black]
    
    mutating func getColor(_ value: Int) -> UIColor {
        var color = UIColor.white
        if colorDictionary[value] == nil {
            colorDictionary[value] = newColor()
        }
        color = colorDictionary[value]!
        return color
    }
    
    private func newColor() -> UIColor {
        let color = UIColor.init(red:   CGFloat(Float(arc4random()) / Float(UINT32_MAX)),
                                 green: CGFloat(Float(arc4random()) / Float(UINT32_MAX)),
                                 blue:  CGFloat(Float(arc4random()) / Float(UINT32_MAX)),
                                 alpha: 1.0)
        return color
    }
}
