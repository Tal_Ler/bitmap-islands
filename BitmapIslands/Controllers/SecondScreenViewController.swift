//
//  ControllerB.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 01/04/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import UIKit

protocol SecondScreenViewControllerCoordinatorDelegate: class {
    func buttonBTapped()
}

protocol SecondScreenViewControllerDelegate: class {
    func secondScreenGetStart()
    func labelTypeCollectinCellChanged(_ labelType: LabelTypeCollectionCell)
}

class SecondScreenViewController: UIViewController {
    
    var bitmap: Bitmap?
    
    // MARK: - IBOutlet
    @IBOutlet weak var islandNmberLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var labelTypeSegmentedControl: UISegmentedControl!
    
    // MARK: - Properties
    let islandNumberText = "Island number is "
    weak var coordinatorDelegate: SecondScreenViewControllerCoordinatorDelegate?
    weak var delegate: SecondScreenViewControllerDelegate?
    
    // MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        button.layer.cornerRadius = 5
        labelTypeSegmentedControl.selectedSegmentIndex = 0
        
        if let bitmapViewController = children.first as? BitmapViewController {
            bitmapViewController.delegate = self
            delegate = bitmapViewController
            delegate?.labelTypeCollectinCellChanged(.value)
            delegate?.secondScreenGetStart()
            labelTypeSegmentedControlChanged(labelTypeSegmentedControl)
        }
    }
    
    // MARK: - IBAction
    @IBAction func buttonTapped(_ sender: Any) {
        coordinatorDelegate?.buttonBTapped()
    }
    
    @IBAction func labelTypeSegmentedControlChanged(_ sender: Any) {
        if let segmentedControl = sender as? UISegmentedControl {
            let labelType = LabelTypeCollectionCell.getType(segmentedControl.selectedSegmentIndex)
            delegate?.labelTypeCollectinCellChanged(labelType)
        }
    }
}

// MARK: - StoryboardInstantiableB
extension SecondScreenViewController: StoryboardInstantiableB {}

// MARK: - BitmapViewControllerDelegate
extension SecondScreenViewController: BitmapViewControllerDelegate {
    
    func colloctionSize() -> (width: Int, height: Int) {
        return ( Int(bitmap?.size.width ?? 10), Int(bitmap?.size.height ?? 10) )
    }
    
    func allowSelection() -> Bool {
        return false
    }
    
    func showSolved() -> Bool {
        return true
    }
    
    func getBitmap() -> [[Int]] {
        return bitmap?.map ?? []
    }
    
    func updateIslandNumber(_ islandNumber: Int) {
        // create formater
        let formatter = NumberFormatter.init()
        formatter.numberStyle = .decimal
        
        // update island number label
        let islandNumberString = formatter.string(from: NSNumber(value: islandNumber) ) ?? "\(islandNumber)"
        DispatchQueue.main.async {
            self.islandNmberLabel?.text =  self.islandNumberText + islandNumberString
        }
    }
    
}
