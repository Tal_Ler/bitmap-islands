//
//  Coordinator.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 31/03/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

protocol Coordinator: class {
    func start()
}
