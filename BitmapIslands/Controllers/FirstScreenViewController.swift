//
//  ControllerA.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 01/04/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import UIKit

protocol FirstScreenViewControllerCoordinatorDelegate: class {
    func firstScreenSolveButtonTapped()
}

protocol FirstScreenViewControllerDelegate: class {
    func firstScreenSliderMoved(_ scale: CGFloat)
    func firstScreenGetBitmap() -> [[Int]]
    func firstScreenGetStart()
}

class FirstScreenViewController: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var bitmapViewContainer: UIView!
    @IBOutlet weak var solveButton: UIButton?
    @IBOutlet weak var slider: UISlider?
    
    // MARK: - Properties
    var bitmap: Bitmap?
    weak var coordinatorDelegate: FirstScreenViewControllerCoordinatorDelegate?
    weak var delegate: FirstScreenViewControllerDelegate?

    
    // MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        solveButton?.layer.cornerRadius = 5
        
        if let bitmapViewController = children.first as? BitmapViewController {
            bitmapViewController.delegate = self
            delegate = bitmapViewController
            delegate?.firstScreenGetStart()
        }
    }
    
    // MARK: - IBAction
    @IBAction func solveButtonTapped(_ sender: Any) {
        if let bitmap = delegate?.firstScreenGetBitmap() {
            self.bitmap?.map = bitmap
        }
        coordinatorDelegate?.firstScreenSolveButtonTapped()
    }
    
    @IBAction func sliderAction(_ sender: UISlider) {
        delegate?.firstScreenSliderMoved(CGFloat(sender.value))
    }
}

// MARK: - StoryboardInstantiableB
extension FirstScreenViewController: StoryboardInstantiableB {}

// MARK: - BitmapViewControllerDelegate
extension FirstScreenViewController: BitmapViewControllerDelegate {

    func colloctionSize() -> (width: Int, height: Int) {
        return ( Int(bitmap?.size.width ?? 10), Int(bitmap?.size.height ?? 10) )
    }
    
    func allowSelection() -> Bool {
        return false
    }
    
    func showSolved() -> Bool {
        return false
    }
    
    func getBitmap() -> [[Int]] {
        return []
    }
    
    func updateIslandNumber(_ islandNumber: Int) {
        
    }
}
