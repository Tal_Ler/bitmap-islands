//
//  StoryboardInstantiable.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 31/03/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import UIKit

// MARK: - Storyboard Instantiable -A-

protocol StoryboardInstantiableA: NSObjectProtocol {
    associatedtype MyType  // 1
    static var defaultFileName: String { get }  // 2
    static func instantiateViewController(_ bundle: Bundle?) -> MyType // 3
}

extension StoryboardInstantiableA where Self: UIViewController {
    static var defaultFileName: String {
        return NSStringFromClass(Self.self).components(separatedBy: ".").last!
    }
    
    static func instantiateViewController(_ bundle: Bundle? = nil) -> Self {
        let fileName = defaultFileName
        let sb = UIStoryboard(name: fileName, bundle: bundle)
        return sb.instantiateInitialViewController() as! Self
    }
}

// MARK: - Storyboard Instantiable -B-

protocol StoryboardInstantiableB {
    static func instantiate() -> Self
}

extension StoryboardInstantiableB where Self: UIViewController {
    static func instantiate() -> Self {
        let id = String(describing: self)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: id)
        return vc as! Self
    }
}
