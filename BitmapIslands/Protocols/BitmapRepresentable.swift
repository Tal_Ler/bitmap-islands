//
//  BitmapRepresentable.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 11/04/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import Foundation

protocol BitmapRepresentable {
    var bitmapValue: Int { get set }
    static func object(with initValue: Int) -> Self
}

extension Int: BitmapRepresentable {
    static func object(with initValue: Int) -> Int {
        return initValue
    }
    
    var bitmapValue: Int {
        get { return self }
        set { self = newValue }
    }
}
extension CellModel: BitmapRepresentable {
    static func object(with initValue: Int) -> CellModel {
        return CellModel(value: initValue)
    }
    
    var bitmapValue: Int {
        get { return value }
        set { value = newValue }
    }
}
