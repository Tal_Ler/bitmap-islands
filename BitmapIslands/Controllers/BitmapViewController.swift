//
//  BitmapViewController.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 31/03/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import UIKit

protocol BitmapViewControllerDelegate: class {
    func colloctionSize() -> (width: Int, height: Int)
    func allowSelection() -> Bool
    func showSolved() -> Bool
    func getBitmap() -> [[Int]]
    func updateIslandNumber(_ islandNumber: Int)
}

fileprivate let minCellSize = CGSize(width: 20.0, height: 20.0)
fileprivate let maxCellSize = CGSize(width: 100.0, height: 100.0)

class BitmapViewController: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contentViewWidth: NSLayoutConstraint!
    @IBOutlet weak var table: UITableView!
    
    // MARK: - Properties
    weak var delegate: BitmapViewControllerDelegate?
    var bitmapCells: [[CellModel]] = []
    var bitmapInts: [[Int]] = []
    var colors = ColorModel()
    var currentContentOffset_X: CGFloat = 0.0
    var bitmapSize: (width: Int, height: Int) = (0, 0)
    var cellSize = CGSize(width: minCellSize.width + (maxCellSize.width - minCellSize.width)/2,
                          height: minCellSize.height + (maxCellSize.height - minCellSize.height)/2)
    var labelType: LabelTypeCollectionCell = .index
    
    let scrollViewThresholdSize = 20
    var useScrollView = true {
        didSet {
            //
            table.isScrollEnabled = !useScrollView
            //
            table.subviews.forEach { (tableCell) in
                if let tableCell = tableCell as? TableCell {
                    tableCell.collection.isScrollEnabled = !useScrollView
                }
            }
            //
            scrollView.isScrollEnabled = useScrollView
        }
    }
    // MARK: - ViewController Lifecycel
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table?.dataSource = self
        table?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func initializeTabel() {
        if let bitmap = delegate?.getBitmap(), bitmap.count > 0 {
            bitmapCells = bitmapConverter(bitmap: bitmap)
            self.bitmapSize = (bitmap[0].count, bitmap.count)
        }
        else {
            if let bitmapSize = delegate?.colloctionSize() {
                self.bitmapSize = bitmapSize
            }
            initializeEmptyBitmap(width: bitmapSize.width, height: bitmapSize.height)
            randomizeBitmap()
        }
        
        if delegate?.showSolved() ?? false {
            showSolvedBitmap()
        }
        
        useScrollView = (bitmapSize.height > scrollViewThresholdSize || bitmapSize.width > scrollViewThresholdSize) ? false : true
        updateContentViewSize()
    }
    
    // MARK: - ...
    
    private func initializeEmptyBitmap(width: Int, height: Int) {
        let c = CellModel(value: 0)
        bitmapCells = get2DimensionalArray(size: CGSize(width: width, height: height) , repeating: c)
        
        DispatchQueue.global().async { [weak self] in
            if let strongSelf = self {
                strongSelf.bitmapInts = get2DimensionalArray(size: CGSize(width: width, height: height), repeating: 0)
            }
        }
        
        useScrollView = (height > scrollViewThresholdSize || width > scrollViewThresholdSize) ? false : true
        updateContentViewSize()
    }
    
    private func randomizeBitmap() {
        
        func theRandomizeBitmap() {
            for i in 0..<bitmapCells.count {
                for j in 0..<bitmapCells[0].count {
                    // A
                    let random = Int.random(in: ClosedRange(uncheckedBounds: (lower: 1, upper: 12)))
                    if (random%3 == 0) {
                        bitmapCells[i][j].value = 1
                    }
                    // B
//                    let random = Int.random(in: ClosedRange(uncheckedBounds: (lower: 0, upper: 1)))
//                    bitmapCells[i][j].value = random
                }
            }
        }
        
        DispatchQueue.global().async { [weak self] in
            theRandomizeBitmap()
            if let strongSelf = self {
                strongSelf.bitmapInts = strongSelf.bitmapConverter(bitmap: strongSelf.bitmapCells)
            }
            DispatchQueue.main.async { [weak self] in
                if let strongSelf = self {
                    strongSelf.table?.reloadData()
                }
            }
        }
    }
    
    private func solveBitmap() {
        //let startTime = CFAbsoluteTimeGetCurrent()
        let solvedBitmapTuple = Logic().findIslands(bitmapCells)
        delegate?.updateIslandNumber(solvedBitmapTuple.islandNumber)
        bitmapCells = solvedBitmapTuple.newBitmap
        //let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
        //print(timeElapsed)
    }
    
    private func showSolvedBitmap() {
        DispatchQueue.global().async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.solveBitmap()
            
            DispatchQueue.main.async { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.table?.reloadData()
            }
        }
    }
    
}

// MARK: - StoryboardInstantiableB
extension BitmapViewController: StoryboardInstantiableB {}

// MARK: - UITableViewDelegate
extension BitmapViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellSize.height + 2  //TODO: TODO - change this '2' value
    }
}

// MARK: - UITableViewDataSource
extension BitmapViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return bitmapCells.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LineID",
                                                 for: indexPath) as! TableCell
        let offset = CGPoint.init(x: currentContentOffset_X,
                                  y: cell.collection?.contentOffset.y ?? 0)
        cell.collection?.setContentOffset(offset, animated: false)
        cell.lineNumber = indexPath.row
        cell.setCollectionView(dataSourceDelegate: self, indexPath: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView,
                   willDisplay cell: UITableViewCell,
                   forRowAt indexPath: IndexPath) {
        if let cell = cell as? TableCell {
            let offset = CGPoint.init(x: currentContentOffset_X,
                                      y: cell.collection?.contentOffset.y ?? 0)
            cell.collection?.setContentOffset(offset, animated: false)
        }
    }
    
}

// MARK: - UICollectionViewDelegate
extension BitmapViewController: UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if abs(currentContentOffset_X - scrollView.contentOffset.x) < cellSize.width {
            currentContentOffset_X = scrollView.contentOffset.x
        }
        for cell in table?.visibleCells ?? [] {
            if let cell = cell as? TableCell {
                let offset = CGPoint.init(x: currentContentOffset_X,
                                          y: cell.collection?.contentOffset.y ?? 0)
                cell.collection?.contentOffset = offset
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        guard delegate?.allowSelection() ?? false else { return }
        
        if let tableCell = collectionView.superview?.superview as? TableCell {
            
            // get for data model
            var cellModel = bitmapCells[tableCell.lineNumber][Int(indexPath.row)]
            cellModel.selectCell()
            
            // update data model
            bitmapCells[tableCell.lineNumber][Int(indexPath.row)] = cellModel
            bitmapInts[tableCell.lineNumber][Int(indexPath.row)] = cellModel.value
            
            // update UI
            let cell = collectionView.cellForItem(at: indexPath) as! CollectionCell
            let v = NSInteger(tableCell.lineNumber)
            cell.updateCell(color: colors.getColor(cellModel.value),
                            indexPath: IndexPath(row: indexPath.row, section: v),
                            value: cellModel.value,
                            labelType: labelType)
            
            // animation
            tableCell.superview!.bringSubviewToFront(tableCell)
            cell.superview!.bringSubviewToFront(cell)
            cell.animateScaleAndBack()
        }
    }
    
}

//MARK: - UICollectionViewDelegateFlowLayout
extension BitmapViewController: UICollectionViewDelegateFlowLayout {
    
    // Getting the Size of Items
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
    
    // Getting the Section Spacing
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}

// MARK: - UICollectionViewDataSource
extension BitmapViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return bitmapCells[0].count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellID",
                                                      for: indexPath) as! CollectionCell
        return cell
    }
 
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        
        if let tableCell = collectionView.superview?.superview as? TableCell {
            let cellModel = bitmapCells[tableCell.lineNumber][Int(indexPath.row)]
            
            if let cell = cell as? CollectionCell {
                let v = NSInteger(tableCell.lineNumber)
                cell.updateCell(color: colors.getColor(cellModel.value),
                                indexPath: IndexPath(row: indexPath.row, section: v),
                                value: cellModel.value,
                                labelType: labelType)
            }
        }
    }
}

// MARK: - Helpers
extension BitmapViewController {

    func bitmapConverter(bitmap: [[Int]]) -> [[CellModel]] {
        let size = CGSize(width: bitmap.count, height: bitmap[0].count)
        var result: [[CellModel]] = get2DimensionalArray(size: size, repeating: CellModel(value: 0))
        for i in 0..<bitmap.count {
            for j in 0..<bitmap[0].count {
                if bitmap[i][j] != 0 {
                    result[i][j].value = bitmap[i][j]
                }
            }
        }
        return result
    }
    
    func bitmapConverter(bitmap: [[CellModel]]) -> [[Int]] {
        let size = CGSize(width: bitmap.count, height: bitmap[0].count)
        var result: [[Int]] = get2DimensionalArray(size: size, repeating: 0)
        for i in 0..<bitmap.count {
            for j in 0..<bitmap[0].count {
                if bitmap[i][j].value != 0 {
                    result[i][j] = bitmap[i][j].value
                }
            }
        }
        return result
    }
    
    func updateContentViewSize() {
        if useScrollView {
            let h = Int(self.cellSize.height + 2) * self.bitmapSize.height
            let w = Int(self.cellSize.width + 1) * self.bitmapSize.width + 1
            
            //        let tempF = CGFloat(40000)
            //        self.contentViewWidth.constant = tempF  //CGFloat(w)
            //        self.contentViewHeight.constant = tempF  //CGFloat(h)
            
            self.contentViewWidth.constant = CGFloat(w)
            self.contentViewHeight.constant = CGFloat(h)
            self.contentView.layoutIfNeeded()
        }
    }
    
    func updateSliderMoved(scale: CGFloat) {    // TODO: TODO: constraint conflict on slider move
        let numberToAdd = scale * (maxCellSize.width - minCellSize.width)
        cellSize = CGSize(width: numberToAdd + minCellSize.width,
                          height: numberToAdd + minCellSize.height)
        table.reloadData()
        updateContentViewSize()
    }
}

// MARK: - FirstScreenViewControllerDelegate
extension BitmapViewController: FirstScreenViewControllerDelegate {
    
    func firstScreenSliderMoved(_ scale: CGFloat) {
        updateSliderMoved(scale: scale)
    }
    
    func firstScreenGetBitmap() -> [[Int]] {
//        return bitmapConverter(bitmap: bitmapCells)
        return bitmapInts
    }
    
    func firstScreenGetStart() {
        initializeTabel()
    }
}

// MARK: - SecondScreenViewControllerDelegate
extension BitmapViewController: SecondScreenViewControllerDelegate {
    
    func secondScreenGetStart() {
        initializeTabel()
    }
    
    func labelTypeCollectinCellChanged(_ labelType: LabelTypeCollectionCell) {
        self.labelType = labelType
        table?.reloadData()
    }
}

// MARK: - DrawScreenViewControllerDelegate
extension BitmapViewController: DrawScreenViewControllerDelegate {
    
    func drawScreenSliderMoved(_ scale: CGFloat) {
        updateSliderMoved(scale: scale)
    }
    
    func drawScreenGetBitmap() -> [[Int]] {
//        return bitmapConverter(bitmap: bitmapCells)
        return bitmapInts
    }
    
    func drawScreenGetStart() {
        if let bitmapSize = delegate?.colloctionSize() {
            self.bitmapSize = bitmapSize
        }
        initializeEmptyBitmap(width: bitmapSize.width, height: bitmapSize.height)
    }
    
    func drawScreenRandomButtonTapped() {
        initializeEmptyBitmap(width: bitmapSize.width, height: bitmapSize.height)
        randomizeBitmap()
    }
}
