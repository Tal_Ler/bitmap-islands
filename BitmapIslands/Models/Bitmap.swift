//
//  Bitmap.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 02/04/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import UIKit

class Bitmap {
    let size: CGSize
    var map: [[Int]]
    
    init(size: CGSize) {
        self.size = size
        map = get2DimensionalArray(size: size, repeating: 0)
    }
}
