//
//  TableCell.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 31/03/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import UIKit

class TableCell: UITableViewCell {

    @IBOutlet weak var collection: UICollectionView!
    var lineNumber = 0
    
    typealias DataSourceDelegate = UICollectionViewDataSource & UICollectionViewDelegate
    func setCollectionView(dataSourceDelegate: DataSourceDelegate, indexPath: IndexPath) {
        collection?.dataSource = dataSourceDelegate
        collection?.delegate = dataSourceDelegate
        collection?.reloadData()
    }
}
