//
//  Logic.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 01/04/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import Foundation

// MARK: - Algorithm -B-
// work good (the used one)

class Logic {
    
    func findIslands<T: BitmapRepresentable>(_ bitmap: [[T]]) -> (newBitmap: [[T]], islandNumber: Int) {
        var currentNumber = T.object(with: 2)
        var result = bitmap
        
        // update the 'result' bitmap variable
        // for each bit in current island (recursively way)
        func recursionBitmap(_ row: Int, _ column: Int) {
            // -- update current bit --
            result[row][column] = currentNumber
            
            // neighbors location
            typealias NeighborLocation = (vertical: Int, horizontal: Int)
            let right:      NeighborLocation = (0,1)
            let rightUp:    NeighborLocation = (-1,1)
            let up:         NeighborLocation = (-1,0)
            let leftUp:     NeighborLocation = (-1,-1)
            let left:       NeighborLocation = (0,-1)
            let leftDown:   NeighborLocation = (1,-1)
            let down:       NeighborLocation = (1,0)
            let rightDown:  NeighborLocation = (1,1)
            
            //
            let neighbors = [right, rightUp, up, leftUp, left, leftDown, down, rightDown]
            let bitmapHight = bitmap.count
            let bitmapWidth = bitmap[0].count
            
            // run clockwise on my neighbors
            for neighbor in neighbors {
                let newColumn = column + neighbor.horizontal
                let newRow = row + neighbor.vertical
                
                let colmunInScope = (newColumn < bitmapWidth) && (newColumn >= 0)
                let rowInScope = (newRow < bitmapHight) && (newRow >= 0)
                let isUnchangedBlack = colmunInScope && rowInScope && result[newRow][newColumn].bitmapValue == 1
                
                if isUnchangedBlack {
                    recursionBitmap(newRow, newColumn)
                }
            }
        }
        
        // linear run on bitmap
        for i in 0..<bitmap.count {
            for j in 0..<bitmap[0].count {
                let value = result[i][j].bitmapValue
                if value == 1 { // black bit that not cahnged
                    result[i][j] = currentNumber
                    recursionBitmap(i, j)   // deep dive all black bit in this island
                    currentNumber.bitmapValue += 1
                }
            }
        }
        
        return (newBitmap: result, islandNumber: currentNumber.bitmapValue-2)
    }
    
}






/*

 // MARK: - Algorithm -A-
 // not worked well (unused)
 
class Logic_2 {
 
    private func getEmptyBitmap(width: Int, height: Int) -> [[Int]] {
        let result = [[Int]](repeating: [Int](repeating: 0, count: width), count: height)
        return result
    }
    
    private func maxValueNeighbors(_ bitmap: [[Int]], row: Int, column: Int) -> Int {
        var res = 0
        
        // right
        if (column+1 < bitmap[0].count) {
            res = max(bitmap[row][column+1], res)
        }
        
        // right-up
        if (column+1 < bitmap[0].count) && (row-1 >= 0) {
            res = max(bitmap[row-1][column+1], res)
        }
        
        // up
        if (row-1 >= 0) {
            res = max(bitmap[row-1][column], res)
        }
        
        // left-up
        if (column-1 >= 0) && (row-1 >= 0) {
            res = max(bitmap[row-1][column-1], res)
        }
        
        // left
        if (column-1 >= 0) {
            res = max(bitmap[row][column-1], res)
        }
        
        // left-down
        if (column-1 >= 0) && (row+1 < bitmap.count) {
            res = max(bitmap[row+1][column-1], res)
        }
        
        // down
        if (row+1 < bitmap.count) {
            res = max(bitmap[row+1][column], res)
        }
        
        // right-down
        if (column+1 < bitmap[0].count) && (row+1 < bitmap.count) {
            res = max(bitmap[row+1][column+1], res)
        }
        
        return res
    }
    
    func findIslands(_ bitmap: [[Int]]) -> [[Int]] {
        var currentNumber = 2
        var result = bitmap
        
        for i in 0..<bitmap.count {
            for j in 0..<bitmap[0].count {
                let value = bitmap[i][j]
                if value == 1 { // black bit that not cahnged
                    
                    // finde the new value
                    let maxVN = maxValueNeighbors(result ,row: i ,column: j)
                    let newValue = (maxVN > 1) ? min(maxVN, currentNumber) : currentNumber
                    
                    // update currentNumber if needed
                    if (newValue == currentNumber) { currentNumber = newValue + 1 }
                    
                    // update bitmap value
                    result[i][j] = newValue
                    
                    // update my neighbors, if my value is biger & they value not 0 or 1
                    updateValueNeighbors(&result, row: i, column: j)
                }
                else if value != 0 {
                    print("corrupted data")
                    //                    result = getEmptyBitmap(width: bitmap[0].count, height: bitmap.count)
                    //                    currentNumber = 2
                    //                    break
                }
            }
        }
        return result
    }
    
    private func updateValueNeighbors(_ bitmap: inout [[Int]], row: Int, column: Int) {
        let centerValue = bitmap[row][column]
        
        func updateIfNeeded(row: Int, column: Int) {
            let value = bitmap[row][column]
            if centerValue > value, value != 0, value != 1 {
                bitmap[row][column] = centerValue
            }
        }
        
        // right
        if (column+1 < bitmap[0].count) {
            updateIfNeeded(row: row, column: column+1)
        }
        
        // right-up
        if (column+1 < bitmap[0].count) && (row-1 >= 0) {
            updateIfNeeded(row: row-1, column: column+1)
        }
        
        // up
        if (row-1 >= 0) {
            updateIfNeeded(row: row-1, column: column)
        }
        
        // left-up
        if (column-1 >= 0) && (row-1 >= 0) {
            updateIfNeeded(row: row-1, column: column-1)
        }
        
        // left
        if (column-1 >= 0) {
            updateIfNeeded(row: row, column: column-1)
        }
        
        // left-down
        if (column-1 >= 0) && (row+1 < bitmap.count) {
            updateIfNeeded(row: row+1, column: column-1)
        }
        
        // down
        if (row+1 < bitmap.count) {
            updateIfNeeded(row: row+1, column: column)
        }
        
        // right-down
        if (column+1 < bitmap[0].count) && (row+1 < bitmap.count) {
            updateIfNeeded(row: row+1, column: column+1)
        }
    }
    
}

 */
