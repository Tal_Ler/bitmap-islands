//
//  DrawScreenViewController.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 02/04/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import UIKit

protocol DrawScreenViewControllerCoordinatorDelegate: class {
    func drawScreenSolveButtonTapped()
}

protocol DrawScreenViewControllerDelegate: class {
    func drawScreenSliderMoved(_ scale: CGFloat)
    func drawScreenGetBitmap() -> [[Int]]
    func drawScreenGetStart()
    func drawScreenRandomButtonTapped()
}

class DrawScreenViewController: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var bitmapViewContainer: UIView!
    @IBOutlet weak var solveButton: UIButton?
    @IBOutlet weak var randomButton: UIButton?
    @IBOutlet weak var slider: UISlider?
    
    // MARK: - Properties
    var bitmap: Bitmap?
    weak var coordinatorDelegate: DrawScreenViewControllerCoordinatorDelegate?
    weak var delegate: DrawScreenViewControllerDelegate?
    
    
    // MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        solveButton?.layer.cornerRadius = 5
        randomButton?.layer.cornerRadius = 5
        
        
        if let bitmapViewController = children.first as? BitmapViewController {
            bitmapViewController.delegate = self
            delegate = bitmapViewController
            delegate?.drawScreenGetStart()
        }
    }
    
    // MARK: - IBAction
    @IBAction func solveButtonTapped(_ sender: Any) {
        if let bitmap = delegate?.drawScreenGetBitmap() {
            self.bitmap?.map = bitmap
        }
        coordinatorDelegate?.drawScreenSolveButtonTapped()
    }
    
    @IBAction func randomButtonTapped(_ sender: Any) {
        delegate?.drawScreenRandomButtonTapped()
    }
    
    @IBAction func sliderAction(_ sender: UISlider) {
        delegate?.drawScreenSliderMoved(CGFloat(sender.value))
    }
}

// MARK: - StoryboardInstantiableB
extension DrawScreenViewController: StoryboardInstantiableB {}

// MARK: - BitmapViewControllerDelegate
extension DrawScreenViewController: BitmapViewControllerDelegate {
    
    func colloctionSize() -> (width: Int, height: Int) {
        return ( Int(bitmap?.size.width ?? 10), Int(bitmap?.size.height ?? 10) )
    }
    
    func allowSelection() -> Bool {
        return true
    }
    
    func showSolved() -> Bool {
        return false
    }
    
    func getBitmap() -> [[Int]] {
        return []
    }
    
    func updateIslandNumber(_ islandNumber: Int) {
        
    }
}
