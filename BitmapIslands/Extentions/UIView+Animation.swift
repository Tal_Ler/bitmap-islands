//
//  UIView+Animation.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 09/04/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import UIKit

extension UIView {
    
    func animateScaleAndBack(duration: TimeInterval = 0.4, scale: CGFloat = 1.2) {
        UIView.animate(
            withDuration: duration/2,
            delay: 0.0,
            usingSpringWithDamping: 1.0,
            initialSpringVelocity: 1.0,
            options: [.allowUserInteraction],
            animations: {
                self.transform = CGAffineTransform(scaleX: scale, y: scale)
                self.layoutIfNeeded()
        }) { _ in
            UIView.animate(
                withDuration: duration/2,
                delay: 0.0,
                usingSpringWithDamping: 1.0,
                initialSpringVelocity: 1.0,
                options: [.allowUserInteraction],
                animations: {
                    self.transform = CGAffineTransform.identity
                    self.layoutIfNeeded()
            },
                completion: nil)
        }
        
    }
    
}
