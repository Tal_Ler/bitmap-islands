//
//  CollectionCell.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 31/03/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import UIKit

enum LabelTypeCollectionCell {
    case empty
    case value
    case index
    
    static func getType(_ value: Int) -> LabelTypeCollectionCell {
        let labelType: LabelTypeCollectionCell
        switch value {
        case 1:
            labelType = .value
        case 2:
            labelType = .index
        default:
            labelType = .empty
        }
        return labelType
    }
}

class CollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel?
    @IBOutlet weak var innerView: UIView?
    
    override func prepareForReuse() {
        backgroundColor = UIColor.black
        innerView?.backgroundColor = UIColor.white
        label?.text = "-"
    }
    
    func updateCell(color: UIColor, indexPath: IndexPath, value: Int, labelType: LabelTypeCollectionCell) {
        innerView?.backgroundColor = color
        
        switch labelType {
        case .empty:
            label?.text = ""
        case .index:
            label?.text = "\(indexPath.section)/\(indexPath.row)"
        case .value:
            label?.text = "\(value)"
        }
    }
    
}
