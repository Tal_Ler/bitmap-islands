//
//  ApplicationCoordinator.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 31/03/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import UIKit

class ApplicationCoordinator: Coordinator {
    
    let window: UIWindow
    let rootViewController: UINavigationController
    var inputViewController: InputViewController?
    
    var bitmap: Bitmap?
    
    init(window: UIWindow) {
        self.window = window
        rootViewController = UINavigationController()
        
        navigateToInputVC(animated: false)
    }
    
    func start() {
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
    }
    
}

// MARK: - Navigation Function
extension ApplicationCoordinator {
    
    func navigateToInputVC(animated: Bool = true) {
        let vc = InputViewController.instantiate()
        vc.coordinatorDelegate = self
        //vc.bitmap = bitmap    // no meaning, passing nil
        inputViewController = vc
        rootViewController.pushViewController(vc, animated: animated)
    }
    
    func navigateToFirstScreenVC(animated: Bool = true) {
        let vc = FirstScreenViewController.instantiate()
        vc.coordinatorDelegate = self
        vc.bitmap = bitmap
        rootViewController.pushViewController(vc, animated: animated)
    }
    
    func navigateToDrawScreenVC(animated: Bool = true) {
        let vc = DrawScreenViewController.instantiate()
        vc.coordinatorDelegate = self
        vc.bitmap = bitmap
        rootViewController.pushViewController(vc, animated: animated)
    }
    
    func navigateToSecondScreenVC(animated: Bool = true) {
        let vc = SecondScreenViewController.instantiate()
        vc.coordinatorDelegate = self
        vc.bitmap = bitmap
        rootViewController.pushViewController(vc, animated: animated)
    }
}

// MARK: - InputViewControllerCoordinatorDelegate
extension ApplicationCoordinator: InputViewControllerCoordinatorDelegate {
    
    func randomButtonTapped() {
        // save bitmap in ApplicationCoordinator - the start (empty) bitmap that initiated in InputViewController
        bitmap = inputViewController?.bitmap
        // navigate to FirstScreenVC
        navigateToFirstScreenVC()
    }
    
    func drawButtonTapped() {
        // save bitmap in ApplicationCoordinator - the start (empty) bitmap that initiated in InputViewController
        bitmap = inputViewController?.bitmap
        // navigate to DrawScreenVC
        navigateToDrawScreenVC()
    }
    
}

// MARK: - FirstScreenViewControllerCoordinatorDelegate
extension ApplicationCoordinator: FirstScreenViewControllerCoordinatorDelegate {

    func firstScreenSolveButtonTapped() {
        navigateToSecondScreenVC()
    }
}

// MARK: - SecondScreenViewControllerCoordinatorDelegate
extension ApplicationCoordinator: SecondScreenViewControllerCoordinatorDelegate {
    
    func buttonBTapped() {
        rootViewController.popToRootViewController(animated: true)
    }
}

// MARK: - DrawScreenViewControllerCoordinatorDelegate
extension ApplicationCoordinator: DrawScreenViewControllerCoordinatorDelegate {
    
    func drawScreenSolveButtonTapped() {
        navigateToSecondScreenVC()
    }
}
