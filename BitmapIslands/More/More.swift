//
//  More.swift
//  BitmapIslands
//
//  Created by Tal Lerman on 11/04/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import UIKit

func get2DimensionalArray<T>(size: CGSize, repeating repeatedValue: T) -> [[T]] {
    let line = [T](repeating: repeatedValue, count: Int(size.height))
    let map = [[T]](repeating: line, count: Int(size.width))
    return map
}
