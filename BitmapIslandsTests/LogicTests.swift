//
//  LogicTests.swift
//  BitmapIslandsTests
//
//  Created by Tal Lerman on 01/04/2019.
//  Copyright © 2019 Tal Lerman. All rights reserved.
//

import XCTest
@testable import BitmapIslands

class LogicTests: XCTestCase {
    let logic = Logic()
    
    // MARK: - MaxValueNeighbors
//
//    func testMaxValueNeighbors_1() {
//
//        let bitmap = [[1,2,3],
//                      [4,5,6],
//                      [7,8,9]]
//
//        var maxValue: Int, expectedResult: Int
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 0, column: 0)
//        expectedResult = 5
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 0, column: 1)
//        expectedResult = 6
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 0, column: 2)
//        expectedResult = 6
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 1, column: 0)
//        expectedResult = 8
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 1, column: 1)
//        expectedResult = 9
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 1, column: 2)
//        expectedResult = 9
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 2, column: 0)
//        expectedResult = 8
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 2, column: 1)
//        expectedResult = 9
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 2, column: 2)
//        expectedResult = 8
//        XCTAssertEqual(maxValue, expectedResult)
//    }
//
//    func testMaxValueNeighbors_2() {
//
//        let bitmap = [[2,0,3,0,0,0,4],
//                      [2,0,3,3,0,0,4],
//                      [2,0,0,0,0,0,4],
//                      [2,0,0,0,0,0,4],
//                      [2,0,0,5,0,0,4],
//                      [2,0,0,0,0,0,4],
//                      [2,0,0,6,6,0,4]]
//
//
//        var maxValue: Int, expectedResult: Int
//
//        //------------------------------------------------------------
//        // row number 0
//        //------------------------------------------------------------
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 0, column: 0)
//        expectedResult = 2
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 0, column: 1)
//        expectedResult = 3
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 0, column: 2)
//        expectedResult = 3
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 0, column: 3)
//        expectedResult = 3
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 0, column: 4)
//        expectedResult = 3
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 0, column: 5)
//        expectedResult = 4
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 0, column: 6)
//        expectedResult = 4
//        XCTAssertEqual(maxValue, expectedResult)
//
//
//        //------------------------------------------------------------
//        // row number 1
//        //------------------------------------------------------------
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 1, column: 0)
//        expectedResult = 2
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 1, column: 1)
//        expectedResult = 3
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 1, column: 2)
//        expectedResult = 3
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 1, column: 3)
//        expectedResult = 3
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 1, column: 4)
//        expectedResult = 3
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 1, column: 5)
//        expectedResult = 4
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 1, column: 6)
//        expectedResult = 4
//        XCTAssertEqual(maxValue, expectedResult)
//
//
//        //------------------------------------------------------------
//        // row number 2
//        //------------------------------------------------------------
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 2, column: 0)
//        expectedResult = 2
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 2, column: 1)
//        expectedResult = 3
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 2, column: 2)
//        expectedResult = 3
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 2, column: 3)
//        expectedResult = 3
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 2, column: 4)
//        expectedResult = 3
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 2, column: 5)
//        expectedResult = 4
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 2, column: 6)
//        expectedResult = 4
//        XCTAssertEqual(maxValue, expectedResult)
//
//
//        //------------------------------------------------------------
//        // row number 3
//        //------------------------------------------------------------
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 3, column: 0)
//        expectedResult = 2
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 3, column: 1)
//        expectedResult = 2
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 3, column: 2)
//        expectedResult = 5
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 3, column: 3)
//        expectedResult = 5
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 3, column: 4)
//        expectedResult = 5
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 3, column: 5)
//        expectedResult = 4
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 3, column: 6)
//        expectedResult = 4
//        XCTAssertEqual(maxValue, expectedResult)
//
//
//        //------------------------------------------------------------
//        // row number 4
//        //------------------------------------------------------------
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 4, column: 0)
//        expectedResult = 2
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 4, column: 1)
//        expectedResult = 2
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 4, column: 2)
//        expectedResult = 5
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 4, column: 3)
//        expectedResult = 0
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 4, column: 4)
//        expectedResult = 5
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 4, column: 5)
//        expectedResult = 4
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 4, column: 6)
//        expectedResult = 4
//        XCTAssertEqual(maxValue, expectedResult)
//
//
//        //------------------------------------------------------------
//        // row number 5
//        //------------------------------------------------------------
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 5, column: 0)
//        expectedResult = 2
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 5, column: 1)
//        expectedResult = 2
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 5, column: 2)
//        expectedResult = 6
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 5, column: 3)
//        expectedResult = 6
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 5, column: 4)
//        expectedResult = 6
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 5, column: 5)
//        expectedResult = 6
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 5, column: 6)
//        expectedResult = 4
//        XCTAssertEqual(maxValue, expectedResult)
//
//
//        //------------------------------------------------------------
//        // row number 6
//        //------------------------------------------------------------
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 6, column: 0)
//        expectedResult = 2
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 6, column: 1)
//        expectedResult = 2
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 6, column: 2)
//        expectedResult = 6
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 6, column: 3)
//        expectedResult = 6
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 6, column: 4)
//        expectedResult = 6
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 6, column: 5)
//        expectedResult = 6
//        XCTAssertEqual(maxValue, expectedResult)
//
//        maxValue = logic.maxValueNeighbors(bitmap, row: 6, column: 6)
//        expectedResult = 4
//        XCTAssertEqual(maxValue, expectedResult)
//    }

    // MARK: - FindIslands
    
    func testFindIslands_1() {
        
        let dataTest = [[1,0,0,0,0,0,1],
                        [1,0,0,0,0,0,1],
                        [1,0,0,0,0,0,1],
                        [1,0,0,0,0,0,1],
                        [1,0,0,0,0,0,1],
                        [1,0,0,0,0,0,1],
                        [1,0,0,0,0,0,1]]
        
        let newBitmap = logic.findIslands(dataTest)
        
        let expectedResult = [[2,0,0,0,0,0,3],
                              [2,0,0,0,0,0,3],
                              [2,0,0,0,0,0,3],
                              [2,0,0,0,0,0,3],
                              [2,0,0,0,0,0,3],
                              [2,0,0,0,0,0,3],
                              [2,0,0,0,0,0,3]]
        
        XCTAssertEqual(newBitmap, expectedResult)
    }
    
    func testFindIslands_2() {
        
        let dataTest = [[1,0,1,0,0,0,1],
                        [1,0,1,1,0,0,1],
                        [1,0,0,0,0,0,1],
                        [1,0,0,0,0,0,1],
                        [1,0,0,1,0,0,1],
                        [1,0,0,0,0,0,1],
                        [1,0,0,1,1,0,1]]
        
        let newBitmap = logic.findIslands(dataTest)
        
        let expectedResult = [[2,0,3,0,0,0,4],
                              [2,0,3,3,0,0,4],
                              [2,0,0,0,0,0,4],
                              [2,0,0,0,0,0,4],
                              [2,0,0,5,0,0,4],
                              [2,0,0,0,0,0,4],
                              [2,0,0,6,6,0,4]]
        
        XCTAssertEqual(newBitmap, expectedResult)
    }
    
    func testFindIslands_3() {
        
        let dataTest = [[1,0,0,0,0,0,1],
                        [1,0,0,0,1,0,1],
                        [1,0,1,1,0,0,1],
                        [1,0,0,0,0,0,1],
                        [1,0,0,0,1,0,1],
                        [1,0,0,1,0,0,1],
                        [1,0,0,0,0,0,1]]
        
        let newBitmap = logic.findIslands(dataTest)
        
//        let expectedResult = [[2,0,0,0,0,0,3],
//                              [2,0,0,0,4,0,3],
//                              [2,0,4,4,0,0,3],
//                              [2,0,0,0,0,0,3],
//                              [2,0,0,0,5,0,3],
//                              [2,0,0,5,0,0,3],
//                              [2,0,0,0,0,0,3]]
        
        let expectedResult = [[2,0,0,0,0,0,3],
                              [2,0,0,0,5,0,3],
                              [2,0,5,5,0,0,3],
                              [2,0,0,0,0,0,3],
                              [2,0,0,0,6,0,3],
                              [2,0,0,6,0,0,3],
                              [2,0,0,0,0,0,3]]
        
        XCTAssertEqual(newBitmap, expectedResult)
    }
    
}
